The collective is based on the commitment to adhere without restraint to the [Manifesto](/en/charter-and-manifesto#Manifesto) and the [Charter](/en/charter-and-manifesto#Charter) of the collective.

These documents are the foundations on which the collective relies for its success.
They are [written](https://framagit.org/framasoft/CHATONS/tree/master) and [amended](https://framagit.org/framasoft/CHATONS/issues) collaboratively by its members.

---
<br/><br/>

# The CHATONS Manifesto

## Preamble

Spectres haunt our digital lives: the centralisation of online services, the insecurity of our personal data, the threats to our freedom of expression and the sharing of information.

Spied upon by multinationals and States, the trails citizens leave on the Internet and the information concerning their behaviours are used to commercial ends or to surveil the masses. Whatever may be the justifications for these uses of our data, it is imperative that we preserve spaces of private communication: spaces which will safeguard our freedoms, beginning with one of the most precious of them all, the freedom to communicate without compromise.

A small number of economic actors of the Internet have over the years reached monopoly positions which imply a centralisation of services. This lack of diversity raises the question of the sovereignty of nations and peoples, as it results in inequalities in access to information, unacceptable censorship, mass gathering of personal data, and selective sorting of information to facilitate the invasion of advertising and marketing.

Extensions of ourselves, our personal data tell about who we are, our political and sexual orientations, our favourite topics, our dreams and objectives. As they constitute essential elements in the private lives of individuals, access to these informations must be decided by individuals themselves according to their own will and in full knowledge. The respect of privacy is today in grave danger not only with regard to protection against malevolent acts, but also from a political, legislative and economic standpoint.

Access to information and communication rely on the assumption that the information carried is verifiable and sustainable at all times. In that perspective, the freedoms held by Free/Libre and Open Source Software (FLOSS) are an essential corollary to the privacy of users, and by extend citizens. Protecting those freedoms amounts to:
  - ensuring the sustainability of information by using free and open formats;
  - decentralising services by multiplying solutions which are accessible, reliable and based on FLOSS;
  - allowing everyone to escape abusive legal clauses locking data into computer systems which do not respect privacy;
  - informing users of all the risks that threaten our freedoms and allowing people to overcome them.

# 1. The CHATONS

Launched in 2014, the "De-google-ify Internet" project of the French non-profit Framasoft has demonstrated, in practice, that it was possible to gather the skills and the means required to decentralise the Internet and, by disseminating the appropriate information, help individuals and organisations do it themselves. Passionate communities have long since demonstrated this through various projects. They promoted both the emergence of new services based on free software and innovation in programming and engineering. Thus appeared a whole network of initiatives, offering users many opportunities to regain control of their data, while preserving their privacy as much as possible. They even contributed in their own way to the emergence of an ever-expanding community of users sharing a vision of freedom and unity, an objective which has always been there in the history of the Internet and hackers since over 40 years.

Hosters who adhere to the principles of FLOSS and attach fundamental importance to individual and social freedoms are invited to participate in a collective called CHATONS: a Collective of Alternative, Transparent, Open, Neutral and United Hosters.

The objective of this collective is to gather initiatives of various online service providers offering FLOSS based solutions, enabling the public to choose services according to their needs. CHATONS members are ad-free, don't monetise user data nor include abusive or obscure legal clauses and therefore ensure a bond of trust.


# 2. Commitments

Members are committed to respect the charter of the collective, whose principles are the following.

## 2.1 Transparency, Non-Discrimination and Personal Data

Probity is key in these commitments, aiming to ensure reliability of offered services and the users' confidence towards them.

The Terms of Service (TOS) must be perfectly clear, accessible and not in contradiction with the CHATONS charter.

The hoster must agree to and display an open policy regarding the administration of user accounts: no discrimination, whether the access be free of charge or not, in full compliance of all applicable laws.
The hoster commits to providing a way for all users to recover their personal data, encrypted or not, except for online services relying on the ephemeral transfer of encrypted personal data.


## 2.2 Openness, Economy, Protection

The offered services must satisfy a few technical requirements. For starters, servers must rely on FLOSS. The software must allow to reproduce the service without requiring additional development relative to the server structure or to the software itself.

Use of open formats is required, at least regarding all data exposed to the users. Therefore, when the use of open formats is impossible (for example because it would imply downloading and installing a new piece of software on the proprietary operating system of the user), the data must instead be made available for a maximum number of operating systems under a free content license. Sources must also be made accessible.

The members of CHATONS commit to respecting the terms of the free software licenses they use (including mentioning these licenses, linking to the source code, etc.).

As a matter of ethics, sponsoring is accepted, so is patronage, donation or having an economic model which consists of asking for payment for some functionalities or even the whole service. The economic model of each member of the CHATONS must be clearly expressed on a dedicated page that the user can easily look up and understand. Obviously, the economic aspects of the activity of all CHATONS members must rigorously conform to the law.

However, it will not be permitted by CHATONS members to make use of advertising coming from an advertising network. No exploitation of personal data can be accomplished, tracking the actions of users will only be made to legal and statistical ends, and the addresses of users can only be used for administrative or technical ends. The statistical tools will also need to be FLOSS and satisfy the conditions of the collective.

If the hoster offers a service or functionalities in exchange of a financial contribution from the user, the fact of offering FLOSS services must not be used only as a commercial argument, but as an ethical one. Likewise, the functionality consisting in the encryption of data cannot be considered as a payable option: encryption is one of the key elements of safeguarding privacy and the freedom to communicate; as such, it is considered as a right and not to be merchandised. If the hoster has the capacity to encrypt (and therefore protect) the data of users, it must be offered as a matter of duty.

## 2.3 Solidarity and Dissemination

Members of CHATONS must help and assist one another, through a dedicated mailing list or other means at their disposal, including confabulations or periodical meetings. This is how the members of CHATONS will be able to improve their services. One of the most effective means to keep mutual assistance alive is by contributing to the software used.

Members of CHATONS must however not stick to themselves and be satisfied with a limited number of users, as this could cause discrimination in the access to services. On the contrary, all communication efforts toward the public are encouraged as a way to disseminate FLOSS based solutions and to create bonds of solidarity around the core principles defended by the collective. These efforts must be mutualised and can take the form of online courses, public information meetings, booths during events, conferences, publishing booklets, etc.

## 2.4 Neutrality

The services of a member of CHATONS cannot be hosted through an actor which, by reputation, does not favour net neutrality.

Data packets must transit without discrimination throughout the services of a member of CHATONS, which means their contents, source and destination must not be inspected.

No communication protocol will be privileged in the distribution of information. The contents of data will not be arbitrarily altered.

Any violation of the principles of net neutrality, either by a member of the collective or by an exterior force affecting a member or the collective as a whole, will have to be reported so as to find a quick solution. If the violation must be publicly denounced, it will be so in the name of the collective only after being approved by a vote of members.

The neutrality of CHATONS is also a political neutrality in the sense that the political convictions of each member will not be examined or sanctioned so long as they do not go beyond respect of all applicable laws.

# 3. General Policy

## 3.1 Public Relations

The collective having no official status, no one will be allowed to speak in its name without first gaining the approval of members through a majority vote. However, every member is encouraged to spread knowledge of the collective freely.

Great importance will be attached to the geographical location and the main characteristics of members of the collective on the chatons.org domain. The website will serve as the point of reference, gathering the aforementioned information for all CHATONS members.

If needed, the collective will be permitted to speak (collectively) through press releases made available on the chatons.org domain: to welcome a new member, to state a position on a news topic, etc.

A logo will be available, if members desire so, and every member will be free to use it or not on their website.

## 3.2 Structures of the Members

The members of CHATONS can be non-profits, individuals, businesses, collectives (non exhaustive list). Essentially:

  * each member will have to appoint a unique delegate or delegation (and inform in case of replacement) who will be the main contact with other members. Either a single person or a group, so long as it is simple and obvious to establish contact (for example, using a single e-mail address).
  * each member of the collective will at least publish a web page presenting the offered services. That URL will serve as reference during exchange with members of the collective.

## 3.3 Functioning of the CHATONS Collective

The CHATONS collective has a mode of management inspired directly by the FLOSS world. Decisions concerning the evolution of the collective and its charter will be taken in a collegial manner. As with source code, the model of the collective can be copied and modified to be adapted (for example to regional needs).

Each member is invited to participate in the collective decisions as much as possible, in a consensual manner. In case of a conflict of opinion, it will be possible to decide by a majority vote.

The chatons.org domain is managed and hosted by Framasoft (for as long as it will be possible and unless the collective decides otherwise). It will present a web site containing the list of members as well as a mailing list allowing exchange between members. The later will be invited to collaborate in the contents published on the site as a way to publicly communicate information relative to CHATONS and FLOSS based hosting.

There is no administrative status for CHATONS, which is primarily a public list of members as well as documentation intended to facilitate the sharing of knowledge, best practices and dissemination.

Any organization respecting the principles of the present manifesto and the CHATONS charter can ask to become a member. To be maintained as a member, the organization will have to communicate to the collective the information regarding its contact person or group and subscribe at least one of its members on the mailing list.

After discussion and potential adjustments, a simple majority vote will be held concerning the entry of the new organisation in the collective.

One or more members can however reserve the right to ask for the exclusion of another member providing the following conditions:

  * to detail the proposition with compelling evidence shared with all members;
  * to accept a collective vote, with or without adversarial debate.

Aware that it is not possible to guaranty the full respect of the CHATONS charter without undermining the confidentiality of personal data hosted on the computer systems of members, the control by peers will *de facto* be imperfect. The collective therefore relies first and foremost upon the trust and the goodwill shared by members.

Rules for managing the inclusion, questioning or exclusion of members of the collective will be done in a collegial manner. Such decision will have to be made in the best interest of the end-users. Fundamental freedoms and privacy of the services offered by the collective are the primal guidelines for any action to be taken.

