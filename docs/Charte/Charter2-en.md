# Charter of CHATONS

## The Collective

CHATONS is a Collective of Independent, Transparent, Open, Neutral and United  Internet Service Providers. Each member of the collective, hereinafter refered to as a "chaton", commits to respecting this here charter. Anything that is not explicitly forbidden by this charter or by law is allowed. The CHATON can take any legal form : natural or legal person (an individual, a company, an association, a cooperative, etc.).

>  **Requirements:**
> - the chaton commits to respectful and kind behaviour towards other members of the collective;
> - the chaton commits to respectful and kind behaviour with regard their users.

## Hosters

A hoster is therin defined as an entity hosting data provided by third parties, and offering services through which the aforementioned data transits or is stored. 
The term "users" mentioned hereinafter refer to the natural or legal persons who can create or modify data within the infrastructure of a chaton.

>  **Requirements:**
> - the CHATON must have root access on the operating system running the final online services;
> - if the CHATON is renting a server (virtual or dedicated), the CHATON must be the sure the provider contractually commits not to access the data;
> - the CHATON must inform visitors and users of the degree of control they have over their infrastructure;
> - the CHATON commits to publicly and unambiguously showing their level of control over the hardware and software that host the services and all related data. Notably, the name of the physical server provider will have to be clearly indicated to the users;
> - the CHATON commits to avoiding service providers -digital services or not- whose practices conflict with the principles of this here charter, particularly regarding the capture and preservation of private data. Should they have to resort to those service providers, the CHATON has to inform their users.

>  **Recommandations:**
> - the CHATON must have complete control over their technical infrastructure (servers and network), their softwares and all related data (root access, hypervisor included). Only the CHATON must be able to technically access the data.

## Independent

By offering FLOSS-based online services, the CHATON offer themselves as an alternative to companies who provide closed and centralized services, resulting in a monopoly and perverse usage of personal data.
Such alternatives illustrate the diversity of available FLOSS-based solutions which can be offered for personal or group usage.

>  **Requirements:**
> - the CHATON commits to publish documentation about the infrastructure and the services provided;
> - the CHATON commits not to implement measures to track how their services are used other than for statistical, technical or administrative purposes;
> - the CHATON commits to favour their users' fundamental freedoms at any given time, especially respecting their privacy;
> - the CHATON will under no circumstances subscribe/implement the services of an advertising agency. Sponsoring or patronizing, through the display of a partner structure's brand identity (name, logo, etc.) is allowed, as long as no personal information is given to the partners;
> - the CHATON must never monetise the data or metadata of the users.

## Transparents

The CHATON undertakes that no unfair usage will ever be done with the users' data, identities or rights, in particular by publishing concise, easy to read and straightforward terms of service.
Users will benefit from available services with a full knowledge of their features, advantages and limits of usage.
The data produced by the means of services is not owned by the chaton. No legal right can be added to the data, and censorship cannot be exercised, as long as the contents complies to the terms of service.

>  **Requirements:**
> - the CHATON commits to be transparent about their technical infrastructure (distribution and software in use). For security reasons, the CHATON may avoid publicly showing some data (software version number, for instance);
> - the CHATON commits to do their best (best-efforts clause) regarding the security of the infrastructure;
> - the CHATON commits to implement the backup policy considered most appropriated and to publicly document it;
> - the CHATON commits to publish logs of all technical issues regarding the services;
> - the CHATON commits not to claim ownership on contents, data and metadata produced by users;
> - the CHATON commits to clearly publish the service offer and the associated price;
> - the CHATON commits to publish visible, clear, unambiguous and widely understandable Terms of Service (ToS);
> - the CHATON commits to publish, within their ToS, a "Personal data and respecting privacy" clause that clearly indicates the CHATON's policy regarding these practices;
> - the CHATON commits to to publish their accounts and activity reports, at least the part regarding their activity as a CHATON;
> - the CHATON commits to make the users aware of the most important information regarding their security and backup policy (while making sure that this information does not cause harm to said policy);
> - the CHATON commits to communicate with users regarding the difficulties they (The CHATON) encounter in the exploitation of their structure and the various services provided, particularly through the implementation of support and bug reports for aforementioned services.

>  **Recommandations:**
> - the CHATON commits to publish anonymized usage statistics;
> - the CHATON commits to publicly publish their accounts, at least the part regarding their activity as a CHATON;

## Open 

The use of FLOSS and open standards for Internet is the only way through which members provide their services. Access to the source code is the core principle of FLOSS. For each service they offer, the chaton commits to use software and code chunks under FLOSS compatible licenses only. Should they improve the code of the software used, the chaton commits to publish improvements under a free license (compatible with the original license), and will encourage every volunteer contribution from users, by inviting them to contact authors. The chaton commits to make the source code available either by publishing a link to the official website of the application, or, if the former is no longer available, by publishing the used source code.

>  **Requirements:**
> - the CHATON commits to use only softwares ruled by FLOSS licenses, as defined by the Free Software Foundation, either compatible or not with the GPL license. Regarding the hardware microcodes to which there are no free functional alternatives, bootloaders (BIOS) and any hardware or software component the CHATON does not have access to, the CHATON commits to publicly list them and their purpose;
> - the CHATON commits to use only FLOSS operating system distributions for the infrastructure hosting the users' data and metadata;
> - the CHATON commits to, on demand, list the packages installed on the servers who host services provided to users;
> - the CHATON commits to use open formats only while carrying out their hosting activities;
> - the CHATON commits to, should they modify the source code of the software used, publish the modifications;
> - the CHATON commits to contribute technically, financially or in other way, to the FLOSS movement;
> - the CHATON commits to make easier for users to leave the services with the related data in open formats;
> - the CHATON commits to delete permanently any information (account and personal data), within the boundaries of legal and technical obligations, regarding the departure of a former user upon his/her request.

>  **Recommandations:**
> - For softwares linked to hardware (BIOS, firmware, drivers, etc.) the CHATON commits to exclusively use those under FLOSS licenses;
> - the CHATON commits to publish their contributions to FLOSS software.

## Neutral

Ethics of FLOSS are based on sharing and independence. The CHATON commits not to censor any content because of preconceptions, no to track users' actions, and to give no reply to any administrative or authoritative request unless it is a legal request in due form. On the users side, they are expected to obey the legal rules of the concerned country when producing or hosting their own contents. Equality of access to those applications shows strong commitment: whether the services provided by the chaton require a subscription fee or not, they must be available without technical or social discrimination, and provide all warranties of neutrality regarding the flow of data. Respecting users' privacy is an imperious demand. No personal data will ever be used for commercial means, forwarded to third parties or used for goals not described by the present charter, except for statistical needs and always within the boundaries of the law.

>  **Requirements:**
> - the CHATON commits to never track users' actions, except for administrative andtechnical means or to improve its internal services;
> - the CHATON commits to make no a priori censorship regarding the contents generated by users;
> - the CHATON commits to protect at best the users' data from external attacks, including by strongly encrypting data as much as possible or during their reception/transmission on the net (SSL/TLS) or their storage (files and databases in particular);
> - the CHATON commits to maintain a list, consultable by its users, regarding administrative or authoritative requests received. Should it be legally impossible to communicate this information, implementing bypassing measures such as Warrant Canary is encouraged ;
> - the CHATON commits not to reply any administrative or authoritative request requiring access to personal information before a legal request in due form is presented;
> - the CHATON commits to apply their ToS with goodwill, if moderation action becomes necessary.

## United

Wirh sharing and mutual assistance in mind, each CHATON should participate in disseminating knowledge to other members and to the general public in order to foster FLOSS usage, and install FLOSS online services. Sharing technical and cognitive resources makes Internet a common good, available for everybody and owned by nobody.

>  **Requirements:**
> - the CHATON commits to, within their means, structurally plan possibilities to physically meet and exchange with users and to make them participate;
> - the CHATON commits to have an active and mindful behaviour regarding access for anybody to the services provided, in particular by complying to standards for web accessibility;
> - the CHATON commits to implement and promote an organisational system which is inclusive, able to accept differences and promote diversity, by taking care that marginalized or excluded groups are involved in the development process;
> - the CHATON commits not to exclude a priori potential subscribers of the featured services. The chaton can define a "targetted audience" to whom they wish to appeal (with criteria based on geographical proximity or interests, for instance). However they must, whenever possible, also address non-pertinent queries, for instance by forwarding them to another chaton or by facilitating the creation of structures fulfilling the expressed needs;
> - the CHATON commits to define an economic model based on solidarity. For paid services, prices must be reasonable and adequate with the implementation costs. In addition, the structure's lowest salary - as an equivalent for a full-time job, comprising primes and dividends - cannot be lower than one fourth of the structure's highest salary - as an equivalent for a full-time job, comprising primes and dividends;
> - the CHATON commits to share their knowledge as widely as possible, publicly and under a FLOSS license;
> - the CHATON commits to facilitate the participation of the users to the maintenance of the services. Particular attention will be paid to "non-technicians" in order to allow them to improve their knowledge and their efficiency and to allow them to contribute fully;
> - the CHATON commits to facilitate the emancipation of their targeted audience, in particular by popular education campaigns (events, meetings, internal or external workshops, codefests, sessions of writing for collaborative documentation, hackathon, etc.) in order to insure that Internet will remain accessible technology for everyone.




