CHATONS - Collective of independant, transparent, open, neutral and ethical hosters providing FLOSS-based (Free/Libre Open Source Software) online services.
------------------------------------------------------------------------

<img alt="logo CHATONS" src="https://framagit.org/chatons/CHATONS/-/raw/master/docs/communication/logo/logo_chatons_v3.1.png" height="300" />

CHATONS is a collective of independant, transparent, open, neutral and ethical hosters providing [FLOSS](https://www.gnu.org/philosophy/floss-and-foss.en.html)-based online services.

**The collective gathers structures that wish to avoid collect and centralisation of personal data within digital silos like GAFAM (Google, Apple, Facebook, Amazon, Microsoft).**

CHATONS has been initiated by Framasoft, in the wake ot the success of its campaign "[De-google-ify Internet](https://degooglisons-internet.org/?l=en)"

A CHATONS member pledges to propose fully FLOSS-based online services, in an ethical and united spirit. CHATONS aims at letting people find easily alternatives to Google products (etc) where privacy concerns are paramount.

To get started with CHATONS, you can:

  * [Discover its current members](https://chatons.org/find)
  * Learn more about the project by reading the [Frequent Asked Questions](https://chatons.org/en/faq)
  * Read the funding principles of the collective: its [Manifest](https://framagit.org/chatons/CHATONS/-/blob/master/docs/Manifesto-en.md) and [Charter](https://framagit.org/chatons/CHATONS/-/blob/master/docs/Charte/Charter2-en.md)
  * Discover our [ambitions](https://framablog.org/2016/02/09/chatons-le-collectif-anti-gafam/)
  * Find out how to [join us](https://chatons.org/rejoindre-le-collectif)
